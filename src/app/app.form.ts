
import { Validators } from '@angular/forms';
import { FormConfig, FormFieldOption, FormFieldType, FormFieldWithOptions } from './interfaces/form.intarface';
import { FormField, FormFieldInputRange } from './utils/form-field';


export const eventFormConfig: FormConfig = {
  id: 'new-event',
  fields: [
    {
      key: 'title',
      type: FormFieldType.Input,
      label: 'Input',
      attrs: {
        type: 'text',
        id: 'event-title',
        name: 'event[title]',
        placeholder: 'Input'
      },
      validators: [Validators.required, Validators.minLength(5)]
    } as FormField,
    {
      key: 'place',
      type: FormFieldType.Checkbox,
      label: 'Checkbox',
      attrs: {
        type: 'text',
        id: 'event-place',
        name: 'event[place]',
        placeholder: 'Checkbox'
      },
      validators: [Validators.required]
    } as FormField,
    {
      key: 'body',
      type: FormFieldType.Number,
      label: 'Number',
      attrs: {
        type: 'text',
        id: 'event-body',
        name: 'event[body]',
        placeholder: 'Number'
      },
      validators: [
        Validators.required, 
        Validators.minLength(50)
      ]
    } as FormField,
  ]
};