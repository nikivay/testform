import { CheckboxComponent } from "./components/field/checkbox/checkbox.component";
import { InputComponent } from "./components/field/input/input.component";
import { RangeComponent } from "./components/field/range/range.component";
import { FormComponent } from "./components/form/form.component";
import { EmptyWrapperComponent } from "./components/wrapper/field-empty-wrapper/field-empty-wrapper.component";
import { FormHostDirective } from "./directives/form-host.directive";
import { RangeDirective } from "./directives/range.directive";
import { FieldWrapperComponent } from "./components/wrapper/field-wrapper/field-wrapper.component";

export const formsLiteComponents: any[] = [FormComponent];

export const formsLiteEntryComponents: any[] = [
  CheckboxComponent,
  FieldWrapperComponent,
  EmptyWrapperComponent,
  InputComponent,
  RangeComponent
];

export const formsLiteDirectives: any[] = [FormHostDirective, RangeDirective];

export const formsLiteExportComponents: any[] = [FormComponent];
