import { FormControl } from "@angular/forms";
import { FormField } from "../utils/form-field";
import { FieldComponent } from "./form.intarface";

/**
 * Abstract field wrapper component
 */
 export abstract class WrapperComponent<T extends FormField = FormField> extends FieldComponent<T> {
  /**
   * Form control
   */
  formControl: FormControl;

  /**
   * Field is group fields
   */
  grouped?: boolean;
}