import { Type } from "@angular/core";
import { AbstractControlOptions, ValidatorFn } from "@angular/forms";
import { FormFieldAttributes, FormFieldHidePayload, FormFieldType, FormFieldWrapperType, RangeOptions } from "../interfaces/form.intarface";

export interface FormField<T = any> {    
    /**
     * Unique key field
     */
    key: string;
  
    /**
     * Form field type
     */
    type: string | FormFieldType | Type<any>;

    /**
     * Html attrs
     */
    attrs?: Partial<FormFieldAttributes>;
    
    /**
     * Default
     */
    defaultValue?: any;   
  
    /**
     * Custom data
     */
    value?: T;
    
    /**
     * Field name
     */
    label: string;

    /**
     * Is static
     */
    static?: boolean;

    /**
     * Is required
     */
    required: boolean;
    
    /**
     * Is show error
     */
    showError?: boolean;

    /**
     * Is hide field
     * @param payload Form field hide payload
     */
    hide?: (payload: Partial<FormFieldHidePayload>) => boolean;

    /**
     * Wrapper component
     */
    wrapper?: string | FormFieldWrapperType | Type<any>;

    /**
     * Field validators
     */
    validators?: ValidatorFn | ValidatorFn[] | AbstractControlOptions | null;
}


export interface FormFieldInputRange<T = any> extends FormField<T> {
    /**
     * Options for range
     */
    rangeOptions: RangeOptions;
  }

  