import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[dyFormHost]'
})
export class FormHostDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
