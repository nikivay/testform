import { Directive, ElementRef, forwardRef, HostListener, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { RangeOptions } from '../interfaces/form.intarface';


@Directive({
  selector: 'input[inputRange]',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RangeDirective),
      multi: true
    }
  ]
})
export class RangeDirective implements ControlValueAccessor {
  /**
   * Range options
   */
  @Input() inputRange: RangeOptions;

  constructor(public elementRef: ElementRef) {}

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {}

  writeValue(obj: any): void {
    if (this.inputRange.defaultValue == null) {
      this.inputRange.defaultValue = 0;
    } else if (this.inputRange.defaultValue > this.inputRange.max) {
      this.inputRange.defaultValue = this.inputRange.max;
    } else if (this.inputRange.defaultValue < this.inputRange.min) {
      this.inputRange.defaultValue = this.inputRange.min;
    }
    this.elementRef.nativeElement.value = this.inputRange.defaultValue;
  }

  @HostListener('blur') onBlur(): void {
    this.onTouched();
  }

  @HostListener('input', ['$event']) onInput(event: any) {
    const value =
      typeof this.inputRange.normalize === 'function'
        ? this.inputRange.normalize(event.target.value, this.inputRange)
        : +event.target.value;
    this.elementRef.nativeElement.value = value;
    this.onChange(value);
  }

  /**
   * On touched handler
   */
  private onTouched = () => {};

  /**
   * On change handler
   */
  private onChange: (value: number) => void = () => {};
}
