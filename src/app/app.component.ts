import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { eventFormConfig } from './app.form';
import { FormfieldControlService } from './services/formfield-control.service';
import { FormField } from './utils/form-field';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [FormfieldControlService]
})
export class AppComponent {
  form = new FormGroup({});

  /**
   * Form config
   */
  formConfig = eventFormConfig;

  /**
   * On changed form
   * @param data Form value
   */
  onChanged<T extends object = Partial<Event>>(data: T): void {
    console.log(data);
  }

  /**
   * On submit
   */
  onSubmit(): void {
    this.form.markAllAsTouched();
  }
}