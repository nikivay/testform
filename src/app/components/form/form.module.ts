import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { FormHostDirective } from 'src/app/directives/form-host.directive';
import { RangeDirective } from 'src/app/directives/range.directive';
import { FormConstructor } from 'src/app/interfaces/form-constructor.interface';
import { BaseFormConstructor } from 'src/app/services/form-constructor.service';
import { CheckboxComponent } from '../field/checkbox/checkbox.component';
import { InputComponent } from '../field/input/input.component';
import { RangeComponent } from '../field/range/range.component';
import { SelectComponent } from '../field/select/select.component';
import { EmptyWrapperComponent } from '../wrapper/field-empty-wrapper/field-empty-wrapper.component';
import { FieldWrapperComponent } from '../wrapper/field-wrapper/field-wrapper.component';
import { FormComponent } from './form.component';


@NgModule({
  imports: [
    CommonModule, 
    FormsModule, 
    ReactiveFormsModule, 
  ],
  declarations: [
    FormComponent,
    CheckboxComponent,
    FieldWrapperComponent,
    EmptyWrapperComponent,
    SelectComponent,
    InputComponent,
    RangeComponent,
    FormHostDirective,
    RangeDirective
  ],
  entryComponents: [
    CheckboxComponent,
    FieldWrapperComponent,
    EmptyWrapperComponent,
    SelectComponent,
    InputComponent,
    RangeComponent
  ],
  exports: [
    FormComponent,
    FormHostDirective, 
    RangeDirective
  ],
  providers: [
    {
      provide: FormConstructor,
      useClass: BaseFormConstructor
    }
  ]
})
export class DynamicFormsModule {}