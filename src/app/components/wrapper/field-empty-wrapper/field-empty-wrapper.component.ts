import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { FormHostDirective } from 'src/app/directives/form-host.directive';
import { WrapperComponent } from 'src/app/interfaces/form-wrapper.interface';

@Component({
  selector: 'ms-field-empty-wrapper',
  templateUrl: './field-empty-wrapper.component.html',
  styleUrls: ['./field-empty-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmptyWrapperComponent extends WrapperComponent {
  /**
   * Form host
   */
  @ViewChild(FormHostDirective, { static: true }) formHost: FormHostDirective;
}
