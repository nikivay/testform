import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmptyWrapperComponent } from './field-empty-wrapper.component';

describe('EmptyWrapperComponent', () => {
  let component: EmptyWrapperComponent;
  let fixture: ComponentFixture<EmptyWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmptyWrapperComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
